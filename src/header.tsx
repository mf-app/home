import * as React from 'react';

interface HeaderProps {
  title: string
}
 
class Hader extends React.Component<HeaderProps> {
  constructor(props: HeaderProps) {
    super(props);
  }

  render() { 
    return <header>{this.props.title}</header>;
  }
}
 
export default Hader;